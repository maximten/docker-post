FROM ubuntu:18.04
RUN apt update && DEBIAN_FRONTEND=noninteractive apt -yq install php7.2
CMD php -r 'echo "hello world\n";'
